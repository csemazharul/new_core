<div class="container-fluid">
   <div class="row">
       <div class="top-bar">
         <p>Call: 123456789 &nbsp;&nbsp;&nbsp; Email: demo.main@gmail.com</p>
       </div>
   </div>
 </div>
<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
 <div class="container">
   <div class="logo">
       <a class="navbar-brand" href="index.html"><img src="{{asset('assets/frontend/images/logo.png')}}" alt="logo"></a>
   </div>
     <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
     <span class="oi oi-menu"></span> Menu
   </button>

   <div class="menu collapse navbar-collapse" id="ftco-nav">
     <ul class="navbar-nav ml-auto">
       <li class="start-li nav-item active"><a href="{{route('homepage')}}" class="nav-link">Home</a></li>
       <li class="nav-item"><a href="{{route('homepage')}}" class="nav-link">About</a></li>
       <li class="nav-item"><a href="{{route('homepage')}}" class="nav-link">Gallery</a></li>
       <li class="nav-item"><a href="{{route('homepage')}}" class="nav-link">Blog</a></li>
       <li class="nav-item"><a href="{{route('contact')}}" class="nav-link">Contact</a></li>
       <li class="nav-item"><a href="{{route('homepage')}}" class="nav-link">Cancel Ticket</a></li>
       <li class="nav-item"><a href="{{route('homepage')}}" class="nav-link">Buy Ticket</a></li>
       <li class="nav-item"><a href="{{route('homepage')}}" class="nav-link">Sign In</a></li>
     </ul>
   </div>
 </div>
</nav>
<div class="hero-wrap js-fullheight" style="background-image: url('assets/frontend/images/body-back.png');">
  <div class="overlay"></div>
  <div class="container">
    <div class="heading row no-gutters slider-text js-fullheight align-items-center justify-content-start" data-scrollax-parent="true">
      <div class="col-md-12 ftco-animate">
        <h1 class="mb-4 heading2"><strong><br></strong>Welcome To The Official WebSite Of <span style="color:#FF2300;">" Shadhin Travels "</span></h1>
        </div>
        <div class="row">
          <div class="col-md-12">
                  <div class="order-form">
                    <div class="buy-ticket">
                      <h1><img src="{{asset('assets/frontend/images/bus2.gif')}}">Buy Ticket</h1>
                      <div class="hr-design"></div>

                    </div>
                    <div class="container form-design">
                      <div class="row">
                        <div class="col-md-12">
                        <form action="{{route('search')}}" method="get">
                          <div class="form-row">
                            <div class="form-group col-md-3">
                              <select id="inputState1" name="start_point" value="" class="form-control">
                                <option selected>Choose...</option>
                                @for($i=0; $i<=count($tripFrom)-1; $i++)
                                    <option value="{{$tripFrom[$i]}}">{{$tripFrom[$i]}}</option>
                                @endfor

                              </select>
                            </div>
                            <div class="form-group col-md-3">
                              <select id="inputState" name="end_point" value="" class="form-control">
                                <option selected>Choose...</option>

                                @for($i=0; $i<=count($tripFrom)-1; $i++)
                                    <option value="{{$tripFrom[$i]}}">{{$tripFrom[$i]}}</option>
                                @endfor

                              </select>
                            </div>
                            <div class="form-group col-md-3">
                              <input type="date" name="date" class="form-control" id="datetimepicker2" placeholder="Date">
                            </div>
                            <div class="form-group col-md-3">
                              <button type="submit" class="btn btn-primary">Search</button>
                            </div>
                          </div>
                        </form>
                        </div>
                      </div>
                    </div>
                  </div>
          </div>
        </div>

        <div class="row also-know">
        @if(request()->path() == '/')

          <div class="col-md-12">
            <p class="browse d-md-flex">
              <span class="d-flex justify-content-md-center align-items-md-center"><a href="#bus-schedule">Schedule</a></span>
              <span class="d-flex justify-content-md-center align-items-md-center"><a href="#bus-route">Route</a></span>
              <span class="d-flex justify-content-md-center align-items-md-center"><a href="#bus-counter">Counters</a></span>
            </p>
          </div>
          @endif
        </div>

    </div>
  </div>
</div>
 <!-- END nav -->
