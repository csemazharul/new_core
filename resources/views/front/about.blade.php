@extends('front.layout.layout')
@section('force-css','bc blog')
@section('style')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/blog.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/bc.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/otherPageResponsive.css')}}">
@endsection
@section('content')
@include('front.layout.header')

    <!-- ============= About Us Area Start ============= -->
    <div class="page-title-container">
        <div class="container">
            <div class="page-title pull-left">
                <h2 class="entry-title">About Us</h2>
            </div>
            <ul class="breadcrumbs pull-right">
                <li><a href="#">HOME</a></li>
                <li class="active">{{__($page_title)}}</li>
            </ul>
        </div>
    </div>

    <section id="content">
        <div class="container">
            <div id="main">
                <div class="image-style style1 large-block">
                    <ul class="image-block column-3 pull-left clearfix">
                        <li><a href="#" class="middle-block"><img class="middle-item" src="images/shortcodes/image/style01/1.png" alt="" width="124" height="88" /></a></li>
                        <li><a href="#" class="middle-block"><img class="middle-item" src="images/shortcodes/image/style01/2.png" alt="" width="124" height="88" /></a></li>
                        <li><a href="#" class="middle-block"><img class="middle-item" src="images/shortcodes/image/style01/3.png" alt="" width="124" height="88" /></a></li>
                        <li><a href="#" class="middle-block"><img class="middle-item" src="images/shortcodes/image/style01/4.png" alt="" width="142" height="102" /></a></li>
                        <li><a href="#" class="middle-block"><img class="middle-item" src="images/shortcodes/image/style01/5.png" alt="" width="136" height="98" /></a></li>
                        <li><a href="#" class="middle-block"><img class="middle-item" src="images/shortcodes/image/style01/6.png" alt="" width="126" height="90" /></a></li>
                        <li><a href="#" class="middle-block"><img class="middle-item" src="images/shortcodes/image/style01/7.png" alt="" width="236" height="91" /></a></li>
                        <li><a href="#" class="middle-block"><img class="middle-item" src="images/shortcodes/image/style01/8.png" alt="" width="212" height="81" /></a></li>
                        <li><a href="#" class="middle-block"><img class="middle-item" src="images/shortcodes/image/style01/9.png" alt="" width="210" height="81" /></a></li>
                    </ul>

                    <h1 class="title">{{__($page_title)}}</h1>
                    <p>{!! $basic->about !!}</p>

                    <div class="clearfix"></div>
                </div>

            </div> <!-- end main -->
        </div>
    </section>
@include('front.layout.footer')
    <!-- ============= About Us Area End ============= -->

@stop
