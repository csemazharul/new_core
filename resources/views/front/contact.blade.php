@extends('front.layout.master')
@section('force-css','index-1')

@section('style')

@stop
@section('content')
    @include('front.layout.header')
    <!-- =========== nav end =========== -->
    <section class="ftco-section contact-section ftco-degree-bg">
    <div class="container">
      <div class="row d-flex mb-5 contact-info">
        <div class="col-md-12 mb-4">
          <h2 class="h4">Contact Information</h2>
        </div>
        <div class="w-100"></div>
        <div class="col-md-3">
          <p><span>Address:</span>{{$basic->address}}</p>
        </div>
        <div class="col-md-3">
          <p><span>Phone:</span> <a href="tel://1234567920">{{$basic->phone}}</a></p>
        </div>
        <div class="col-md-3">
          <p><span>Email:</span> <a href="mailto:info@yoursite.com">{{$basic->email}}</a></p>
        </div>
        <div class="col-md-3">
          <p><span>Website</span> <a href="{{url('/')}}">{{$basic->sitename}}</a></p>
        </div>
      </div>
      <div class="row block-9">
        <div class="col-md-6 pr-md-5">
          <form action="#">
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Your Name">
            </div>
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Your Email">
            </div>
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Subject">
            </div>
            <div class="form-group">
              <textarea name="" id="" cols="30" rows="7" class="form-control" placeholder="Message"></textarea>
            </div>
            <div class="form-group">
              <input type="submit" value="Send Message" class="btn btn-primary py-3 px-5">
            </div>
          </form>

        </div>

        <div class="col-md-6">

          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d775.6419872996584!2d91.83243958110333!3d22.368883000871975!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30ad277fb863e7c7%3A0xca7815554909801c!2sMuradpur%20Bus%20Stop!5e0!3m2!1sen!2sbd!4v1581948487649!5m2!1sen!2sbd" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>

        </div>
      </div>
    </div>
  </section>

    @include('front.layout.footer')
@stop


@section('script')
     <!-- <script type="text/javascript" src="{{asset('')}}assets/frontend/js/jquery-1.11.1.min.js"></script> -->
    <!-- <script type="text/javascript">var jquery_2_2 = $.noConflict(true);</script> -->
    <script src="{{asset('assets/front/js/flatpickr.js')}}"></script>

@stop
@section('js')
<script type="text/javascript">
  $("#datetimepicker2").flatpickr({
      minDate: "today",
      maxDate: new Date().fp_incr(50), // 14 days from now
      dateFormat: "d M Y",
  });

</script>
@stop
