<!--<!DOCTYPE html>-->
<!--<html lang="en" dir="ltr">-->
<!--<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">-->

<!--    <title>{{$basic->sitename}}</title>-->
<!--    <link rel="stylesheet" href="{{asset('assets/front/css/bootstrap.min.css')}}" >-->
<!--    <link rel="stylesheet" href="{{asset('assets/admin/css/fontawesome-all.min.css')}}">-->

<!--    <style>-->

<!--        .page-break {-->
<!--            page-break-after: always;-->
<!--        }-->
<!--        .ticket-header {-->
<!--            background: #000036;-->
<!--            width: 100%;-->
<!--        }-->
<!--        .ticket-footer{-->
<!--            width: 100%;-->
<!--        }-->
<!--        .company-logo {-->
<!--            display: inline-block;-->
<!--        }-->
<!--        .company-logo img {-->
<!--            position: relative;-->
<!--            margin-left: 65px;-->
<!--            margin-top: 25px;-->
<!--        }-->
<!--        .company-short {-->
<!--            background: #391261;-->
<!--            color: white;-->
<!--            display: inline-block;-->
<!--            position: relative;-->
<!--            margin-left: 20%;-->
<!--            top: 20px;-->
<!--        }-->
<!--        ul > li{-->
<!--            font-size: 16px;-->
<!--        }-->

<!--        @media print {-->

<!--            body {-->
<!--                -webkit-print-color-adjust: exact !important;-->
<!--            }-->
<!--            #printTic{-->
<!--                display: block;-->
<!--            }-->
<!--            .btn-print{-->
<!--                display: none;-->
<!--            }-->
<!--            .page-break {-->
<!--                page-break-after: always;-->
<!--            }-->
<!--            .ticket-header {-->
<!--                background: #391261;-->
<!--                width: 100%;-->
<!--            }-->
<!--            .ticket-footer{-->
<!--                width: 100%;-->
<!--            }-->
<!--            .company-logo {-->
<!--                display: inline-block;-->
<!--            }-->
<!--            .company-logo img {-->
<!--                position: relative;-->
<!--                margin-left: 65px;-->
<!--                margin-top: 25px;-->
<!--            }-->
<!--            .company-short {-->
<!--                background: #391261;-->
<!--                color: white;-->
<!--                display: inline-block;-->
<!--                position: relative;-->
<!--                margin-left: 20%;-->
<!--                top: 20px;-->
<!--            }-->
<!--            ul > li{-->
<!--                font-size: 16px;-->
<!--            }-->

<!--        }-->
<!--    </style>-->

<!--</head>-->
<!--<body style="height: 400px;width: 100%">-->

<!--<button style="margin-top: 20px; margin-left: 30px" class="btn btn-sm btn-success btn-lg btn-print" onClick="window.print();"> <i class="fa fa-print"></i> Print</button>-->
<!--<span style="clear: both"></span>-->

<!--<div class="container" id="printTic">-->

<!--    <div class="row">-->
<!--        <div class="col-md-12">-->

<!--            <div class="ticket-header margin-top-30 text-center">-->
<!--                <div class="company-logo"><br>-->
<!--                    <img src="{{asset('assets/images/logo/logo.png')}}" alt="{{$basic->sitename}}" style="max-height: 60px; max-width: 280px;"><br><br>-->
<!--                </div>-->


<!--            </div>-->

<!--            <div class="ticket-body" style="border:2px solid #000036;">-->
<!--                <div style="padding-top: 30px">-->
<!--                    <div class="body-left" style="width: 40%; float: left;display: inline-block;position: relative; margin-left: 10%;">-->
<!--                        <ul style="list-style: none;padding: 0; margin: 0;">-->
<!--                            <li style="padding: 5px;"><h6>Name: <strong>@isset($passenger_name) {{$passenger_name}} @endisset</strong></h6> </li>-->
<!--                            <li style="padding: 5px;"><h6>Mob: <strong>@isset($passenger_number) {{$passenger_number}} @endisset</strong></h6></li>-->
<!--                            <li style="padding: 4px;">Departure Time: <b style="margin-left: 30px"> @isset($departureTime) {{$departureTime}} @endisset</b></li>-->
<!--                            <li style="padding: 4px;">Seat Fare : <b style="margin-left: 30px">@isset($seat_fare) {{$seat_fare}} {{$basic->currency}} @endisset</b></li>-->
<!--                            <li style="padding: 4px;">Seats: <b style="margin-left: 30px"> @isset($seats) {{$seats}} @endisset</b></li>-->
<!--                            <li style="padding: 4px;">From: <b style="margin-left: 30px"> @isset($from) {{$from}} @endisset</b></li>-->
<!--                            <li style="padding: 4px;">Boarding: <b style="margin-left: 30px"> @isset($boarding) {{$boarding}} @endisset </b></li>-->
<!--                            <li style="padding: 4px;">Issued On: <b style="margin-left: 30px">  @isset($issueOn) {{$issueOn}} @endisset </b></li>-->
<!--                        </ul>-->
<!--                    </div>-->
<!--                    <div class="body-right" style="width: 45%; display: inline-block;float: left;">-->
<!--                        <ul style="list-style: none;padding: 0; margin: 0;">-->
<!--                            <li style="padding: 5px;"> <h6>PNR: <strong>@isset($pnr) {{$pnr}} @endisset</strong></h6>  </li>-->
<!--                            <li style="padding: 4px;">Coach: <b style="margin-left: 30px"> @isset($coach) {{$coach}} @endisset</b></li>-->
<!--                            <li style="padding: 4px;">Journey Date: <b style="margin-left: 30px">@isset($journeyDate) {{$journeyDate}} @endisset</b></li>-->
<!--                            <li style="padding: 4px;">Reporting Time: <b style="margin-left: 30px">@isset($reportingTime) {{$reportingTime}} @endisset</b></li>-->

<!--                            <li style="padding: 4px;">Total Fare: <b style="margin-left: 30px">@isset($total_fare) {{$total_fare}}  {{$basic->currency}} @endisset </b></li>-->
<!--                            <li style="padding: 4px;">To: <b style="margin-left: 30px">@isset($to) {{$to}} @endisset</b></li>-->
<!--                            <br>-->
<!--                            <li style="padding: 4px;">Issued By: <b style="margin-left: 30px">@isset($issueBy) {{$issueBy}} @endisset</b></li>-->
<!--                        </ul>-->
<!--                    </div>-->
<!--                    <p style="text-align: center; padding: 20px 80px 30px; font-weight: bold; clear: both">-->
<!--                        Buy tickets at home <strong>{{url('/')}}</strong>-->
<!--                    </p>-->
<!--                </div>-->
<!--            </div>-->

<!--            <div class="ticket-footer" style="background: #000036; color:#fff; text-align: center;margin-top: -20px;"><br>-->
<!--                <p><b>Software Developer By : Rcreation || 01722964303,01813316786</b> </p><br>-->

<!--            </div>-->

<!--        </div>-->
<!--    </div>-->
<!--</div>-->




<!-- jquery -->
<!--<script src="{{asset('assets/front/js/jquery.js')}}"></script>-->
<!-- bootstrap -->
<!--<script src="{{asset('assets/front/js/bootstrap.min.js')}}"></script>-->

<!--</body>-->
<!--</html>-->



<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>

    <title>{{$basic->sitename}}</title>
    <link rel="stylesheet" href="{{asset('assets/front/css/bootstrap.min.css')}}" >
    <link rel="stylesheet" href="{{asset('assets/admin/css/fontawesome-all.min.css')}}">


<style type="text/css">
.card{margin: 0 auto;  width: 1000px;  }
	.main_table{
	/*background-color:#f2f2f280;*/
	height: 350px;


}
@media print
{
    .btn-print
    {
        display: none !important;
    }
}

	.main_table tr td{
		text-align:left;


			 vertical-align: baseline;
			 padding-top: 60px;
			  text-transform: capitalize;


	}


		.child_table tr td{

			padding:2px 6px;
			font-size: 12px;


		}


		.main_table_span_phone{
          	font-size: 11px!important;
    	text-align: center;
    		color:green;
			}


		.main_table_span{
          	font-size: 10px!important;
    	text-align: center;
    	margin-top:10px;
    	margin-bottom:0px;
			}
			.main_table_span_a{
			text-decoration: none!important;
			}

      .main_table_p{
      	margin-bottom: -10px;
      }
.main_table_image{
	margin-top: -12px;margin-left: -11px;height: 79px;margin-bottom: -20px; width: 160px;
}
.main_table_span_1{
	position: absolute;
				margin-left: 20px;color: #fff;margin-top: 12px;
}
.main_table_span_2{
	position: absolute;
				margin-left: 10px;color: #fff;margin-top: 15px;
				font-size: 12px;
}

.card{
position: relative;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-direction: column;
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    background-color: #fff;
    background-clip: border-box;
     border: none!important;
    border-radius: .25rem;
}

	</style>


<body>

<button style="margin-top: 10px; margin-left: 30px" class="btn btn-sm btn-success btn-lg btn-print" onClick="window.print();"> <i class="fa fa-print"></i> Print</button>
<div  class="card">
    <table class="main_table">
            <tr>
	<td style="width: 450px;">
						<table style="height: 265px;  margin-left: -30px" class="child_table">
                    <tr>
                        <td>Name</td>
                        <td><b>:@isset($passenger_name) {{$passenger_name}} @endisset</b></td>
                        <td>PNR</td>
                        <td><b>: @isset($pnr) {{$pnr}} @endisset</b></td>

                    </tr>
                    <tr>
                        <td>Mob</td>
                        <td><b>:@isset($passenger_number) {{$passenger_number}} @endisset</b></td>
                        <td>Coach</td>
                        <td><b>: @isset($coach) {{$coach}} @endisset</b></td>
                    </tr>
                    <tr>
                        <td>Departure Time</td>
                        <td><b>: @isset($departureTime) {{$departureTime}} @endisset</b></td>
                        <td>Journey Date</td>
                        <td><b>: @isset($journeyDate) {{$journeyDate}} @endisset</b></td>
                    </tr>
                    <tr>
                        <td>Seat Fare</td>
                        <td><b>: @isset($seat_fare) {{$seat_fare}} {{$basic->currency}} @endisset</b></td>
                        <td>Total Fare</td>
                        <td><b>: @isset($total_fare) {{$total_fare}}  {{$basic->currency}} @endisset</b></td>
                    </tr>
                    <tr>
                        <td>Seats</td>
                        <td><b>: @isset($seats) {{$seats}} @endisset</b></td>
                        <td>To</td>
                        <td><b>: @isset($to) {{$to}} @endisset</b></td>

                    </tr>
                    <tr>
                        <td>From</td>
                        <td><b>: @isset($from) {{$from}} @endisset</b></td>
                        <td>Issued By</td>
                        <td><b>: @isset($issueBy) {{$issueBy}} @endisset</b></td>

                    </tr>
                    <tr>
                        <!--<td> Boarding</td>-->
                        <!--<td><b>: @isset($boarding) {{$boarding}} @endisset</b></td>-->
                        <td>Issued On</td>
                        <td><b>: @isset($issueOn) {{$issueOn}} @endisset</b></td>


                    </tr>


                </table>

					<p class="main_table_span"><b>Software Developed and Maintaince By <a class="main_table_span_a" style="color:red" target="_blank" href="https://rcreation-bd.com/"> R-Creation</a></b> </p>
					<p  class="main_table_span_phone"><b style="margin-bottom:0px">+880-1722-964303,+880-1813-316786
</b> </p>

            </td>
            <td style="width: 290px; padding-left: 10px" >
                   	<table style="height: 265px" class="child_table">
                <tr>
                    <td>Name</td>
                    <td><b>: @isset($passenger_name) {{$passenger_name}} @endisset</b></td>

                </tr>
                <tr>
                    <td>Mob</td>
                    <td><b>: @isset($passenger_number) {{$passenger_number}} @endisset</b></td>

                </tr>
                <tr>

                    <td>Journey Route</td>
                  <td><b>: @isset($from) {{$from}} @endisset</b>-<b>: @isset($to) {{$to}} @endisset</b></td>
                </tr>
                <tr>
                    <td>Departure Time</td>
                    <td><b>: @isset($departureTime) {{$departureTime}} @endisset</b></td>

                </tr>
                 <tr>
                    <td>Seats</td>
                    <td><b>: @isset($seats) {{$seats}} @endisset</b></td>

                </tr>
                <tr>
                    <td>Seat Fare</td>
                    <td><b>: @isset($seat_fare) {{$seat_fare}} {{$basic->currency}} @endisset</b></td>

                </tr>
                <tr>
                    <td>Total Fare</td>
                    <td><b>: @isset($total_fare) {{$total_fare}}  {{$basic->currency}} @endisset</b></td>
                </tr>

                <tr>
                    <td>From</td>
                    <td><b>: @isset($from) {{$from}} @endisset</b></td>

                </tr>
                <!--<tr>-->
                <!--    <td> Boarding</td>-->
                <!--    <td><b>: @isset($boarding) {{$boarding}} @endisset</b></td>-->

                <!--</tr>-->
                <tr>
                    <td>Issued On</td>
                    <td><b>: @isset($issueOn) {{$issueOn}} @endisset</b></td>

                </tr>
                <tr>

                    <td>Journey Date</td>
                    <td><b>: @isset($journeyDate) {{$journeyDate}} @endisset</b></td>
                </tr>

                <tr>
                    <td>PNR</td>
                    <td><b>: @isset($pnr) {{$pnr}} @endisset</b></td>

                </tr>
                <tr>

                    <td>Coach</td>
                    <td><b>: @isset($coach) {{$coach}} @endisset</b></td>
                </tr>

            </table>
        <p class="main_table_span"><b>Software Developed and Maintaince By <a class="main_table_span_a" style="color:red" target="_blank" href="https://rcreation-bd.com/"> R-Creation</a></b> </p>
					<p class="main_table_span_phone"><b>+880-1722-964303,+880-1813-316786</b> </p>
            </td>
					<td style="width: 300px;
    padding-left: 35px; margin-left:50px">
                   	<table style="height: 265px" class="child_table">
                    <tr>
                        <td>Name</td>
                        <td><b>: @isset($passenger_name) {{$passenger_name}} @endisset</b></td>

                    </tr>
                    <tr>
                        <td>Mob</td>
                        <td><b>: @isset($passenger_number) {{$passenger_number}} @endisset</b></td>

                    </tr>
                <tr>

                    <td>Journey Route</td>
                  <td><b>: @isset($from) {{$from}} @endisset</b>-<b>: @isset($to) {{$to}} @endisset</b></td>
                </tr>
                    <tr>
                        <td>Departure Time</td>
                        <td><b>: @isset($departureTime) {{$departureTime}} @endisset </b></td>

                    </tr>
                    <tr>
                        <td>Seats</td>
                        <td><b>: @isset($seats) {{$seats}} @endisset </b></td>

                    </tr>
                    <tr>
                        <td>Seat Fare</td>
                        <td><b>: @isset($seat_fare) {{$seat_fare}} {{$basic->currency}} @endisset</b></td>

                    </tr>
                    <tr>
                        <td>Total Fare</td>
                        <td><b>: @isset($total_fare) {{$total_fare}}  {{$basic->currency}} @endisset</b></td>

                    </tr>
                    <tr>
                        <td>From</td>
                        <td><b>: @isset($from) {{$from}} @endisset</b></td>

                    </tr>
                    <!--<tr>-->
                    <!--    <td> Boarding</td>-->
                    <!--    <td><b>: @isset($boarding) {{$boarding}} @endisset</b></td>-->

                    <!--</tr>-->
                    <tr>
                        <td>Issued On</td>
                        <td><b>: @isset($issueOn) {{$issueOn}} @endisset</b></td>

                    </tr>
                    <tr>

                        <td>Journey Date</td>
                        <td><b>: @isset($journeyDate) {{$journeyDate}} @endisset</b></td>
                    </tr>

                    <tr>
                        <td>PNR</td>
                        <td><b>: @isset($pnr) {{$pnr}} @endisset</b></td>

                    </tr>
                    <tr>
                        <td>Coach</td>
                        <td><b>: @isset($coach) {{$coach}} @endisset</b></td>
                    </tr>

                </table>
                <p class="main_table_span"><b>Software Developed and Maintaince By <a class="main_table_span_a" style="color:red" target="_blank" href="https://rcreation-bd.com/"> R-Creation</a></b> </p>
					<p class="main_table_span_phone"><b>+880-1722-964303,+880-1813-316786</b> </p>
            </td>
        </tr>
    </table>
</div>



<!--<div  class="card">-->
<!--		<table class="main_table">-->
<!--		<tr>-->
<!--			<td style="width: 450px;  ">-->
<!--						<table style="height: 265px;  margin-left: -10px" class="child_table">-->
<!--						<tr>-->
<!--							<td>Name</td>-->
<!--			                <td><b>: Mahbub</b></td>-->
<!--			                <td>PNR</td>-->
<!--			                <td><b>: 2NL0MHLMSGWB</b></td>-->

<!--						</tr>-->
<!--						<tr>-->
<!--							<td>Mob</td>-->
<!--			                    <td><b>: 01788555115</b></td>-->
<!--			              <td>Coach</td>-->
<!--			                <td><b>: 102</b></td>-->
<!--						</tr>-->
<!--						<tr>-->
<!--							<td>Departure Time</td>-->
<!--			                   <td><b>: 10:53 AM</b></td>-->
<!--			            <td>Journey Date</td>-->
<!--			                <td><b>: 11:07 AM</b></td>-->
<!--						</tr>-->
<!--						<tr>-->
<!--							<td>Seat Fare</td>-->
<!--			                <td><b>: 350 BDT</b></td>-->
<!--			              <td>Total Fare</td>-->
<!--			                <td><b>: 700.00 BDT</b></td>-->
<!--						</tr>-->
<!--						<tr>-->
<!--							<td>Seats</td>-->
<!--			            <td><b>: G1, G2,</b></td>-->
<!--			            <td>To</td>-->
<!--			                <td><b>: Coxsbazar</b></td>-->

<!--						</tr>-->
<!--						<tr>-->
<!--							<td>From</td>-->
<!--			                   <td><b>: Chattogram</b></td>-->
<!--			                   <td>Issued By</td>-->
<!--			                <td><b>: karim</b></td>-->

<!--						</tr>-->
<!--						<tr>-->
<!--							<td> Boarding</td>-->
<!--			               <td><b>: </b></td>-->
<!--			               <td>Issued On</td>-->
<!--			                <td><b>: 09 Dec 2019</b></td>-->


<!--						</tr>-->


<!--					</table>-->

<!--					<p class="main_table_span"><b>Developed By <a class="main_table_span_a" style="color:red" target="_blank" href="https://rcreation-bd.com/"> R-Creation</a></b> </p> -->
<!--					<p class="main_table_span_phone"><b>+880-1722-964303,+880-1813-316786-->
<!--</b> </p> -->
<!--				</td>-->
<!--				<td style="width: 290px; padding-left: 10px" >-->
<!--                   	<table style="height: 265px" class="child_table">-->
<!--						<tr>-->
<!--							<td>Name</td>-->
<!--			                <td><b>: Mahbub</b></td>-->

<!--						</tr>-->
<!--						<tr>-->
<!--							<td>Mob</td>-->
<!--			                    <td><b>: 01788555115</b></td>-->

<!--						</tr>-->
<!--						<tr>-->
<!--							<td>Departure Time</td>-->
<!--			                   <td><b>: 10:53 AM</b></td>-->

<!--						</tr>-->
<!--						<tr>-->
<!--							<td>Seat Fare</td>-->
<!--			                <td><b>: 350 BDT</b></td>-->

<!--						</tr>-->
<!--						<tr>-->
<!--							<td>Seats</td>-->
<!--			            <td><b>: G1, G2,</b></td>-->

<!--						</tr>-->
<!--						<tr>-->
<!--							<td>From</td>-->
<!--			                   <td><b>: Chattogram</b></td>-->

<!--						</tr>-->
<!--						<tr>-->
<!--							<td> Boarding</td>-->
<!--			               <td><b>: </b></td>-->

<!--						</tr>-->
<!--						<tr>-->
<!--							<td>Issued On</td>-->
<!--			              <td><b>: 09 Dec 2019</b></td>-->

<!--						</tr>-->
<!--						<tr>-->

<!--			            <td>Journey Date</td>-->
<!--			                <td><b>: 11:07 AM</b></td>-->
<!--						</tr>-->

<!--			            	<tr>-->
<!--							 <td>PNR</td>-->
<!--			                <td><b>: 2NL0MHLMSGWB</b></td>-->

<!--						</tr>-->
<!--						<tr>-->

<!--			            <td>Coach</td>-->
<!--			                <td><b>: 102</b></td>-->
<!--						</tr>-->

<!--					</table>-->
<!--						<p class="main_table_span"><b>Developed By <a class="main_table_span_a" style="color:red" target="_blank" href="https://rcreation-bd.com/"> R-Creation</a></b> </p> -->
<!--					<p class="main_table_span_phone"><b>+880-1722-964303,+880-1813-316786</b> </p> -->
<!--				</td>-->
<!--					<td style="width: 250px;-->
<!--    padding-left: 35px;">-->
<!--                   	<table style="height: 265px" class="child_table">-->
<!--						<tr>-->
<!--							<td>Name</td>-->
<!--			                <td><b>: Mahbub</b></td>-->

<!--						</tr>-->
<!--						<tr>-->
<!--							<td>Mob</td>-->
<!--			                    <td><b>: 01788555115</b></td>-->

<!--						</tr>-->
<!--						<tr>-->
<!--							<td>Departure Time</td>-->
<!--			                   <td><b>: 10:53 AM</b></td>-->

<!--						</tr>-->
<!--						<tr>-->
<!--							<td>Seat Fare</td>-->
<!--			                <td><b>: 350 BDT</b></td>-->

<!--						</tr>-->
<!--						<tr>-->
<!--							<td>Seats</td>-->
<!--			            <td><b>: G1, G2,</b></td>-->

<!--						</tr>-->
<!--						<tr>-->
<!--							<td>From</td>-->
<!--			                   <td><b>: Chattogram</b></td>-->

<!--						</tr>-->
<!--						<tr>-->
<!--							<td> Boarding</td>-->
<!--			               <td><b>: </b></td>-->

<!--						</tr>-->
<!--						<tr>-->
<!--							<td>Issued On</td>-->
<!--			              <td><b>: 09 Dec 2019</b></td>-->

<!--						</tr>-->
<!--						<tr>-->

<!--			            <td>Journey Date</td>-->
<!--			                <td><b>: 11:07 AM</b></td>-->
<!--						</tr>-->

<!--			                 	<tr>-->
<!--							 <td>PNR</td>-->
<!--			                <td><b>: 2NL0MHLMSGWB</b></td>-->

<!--						</tr>-->
<!--						<tr>-->

<!--			            <td>Coach</td>-->
<!--			                <td><b>: 102</b></td>-->
<!--						</tr>-->

<!--					</table>-->
<!--						<p class="main_table_span"><b>Developed By <a class="main_table_span_a" style="color:red" target="_blank" href="https://rcreation-bd.com/"> R-Creation</a></b> </p> -->
<!--					<p class="main_table_span_phone"><b>+880-1722-964303,+880-1813-316786</b> </p> -->
<!--				</td>-->
<!--			</tr>-->
<!--		</table>-->
<!--</div>-->





<!-- jquery -->
<script src="{{asset('assets/front/js/jquery.js')}}"></script>
<!-- bootstrap -->
<script src="{{asset('assets/front/js/bootstrap.min.js')}}"></script>

</body>
</html>
