@extends('layout')
@section('body_class', 'soap-login-page style1 body-blank')
@section('content')

    <!--================================
        contact us  part start
    =====================================-->
    <!-- <section id="contact-main">

        <div class="contact-form">
            <div class="container">
                <div class="row contact-form-area">
                    <div class="col-lg-8 offset-2 contact-form">


                        @if (session('logout'))
                            <div class="alert alert-danger alert-dismissible">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                {{ session('logout') }}
                            </div>
                        @endif
                        @if (session('success'))
                            <div class="alert alert-success alert-dismissible">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                {{ session('success') }}
                            </div>
                        @endif
                        @if (session('danger'))
                            <div class="alert alert-danger alert-dismissible">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                {{ session('danger') }}
                            </div>
                        @endif

                        <form id="c-form" action="{{route('login')}}" method="post">
                            @csrf

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <h5>Username</h5>
                                        <input type="text" name="username" value="{{old('username')}}" class="form-control" placeholder="Enter Username">
                                        @if ($errors->has('username'))
                                            <strong class="error">{{ $errors->first('username') }}</strong>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <h5>Password</h5>
                                    <input type="password" name="password" class="form-control" placeholder="Enter Password">
                                    @if ($errors->has('password'))
                                        <strong class="error">{{ $errors->first('password') }}</strong>
                                    @endif
                                </div>
                                <div class="col-12">
                                    <button type="submit" class="btn btn-contact btn-block">Sign In</button>
                                </div>
                            </div>



                            <div class="form-row  margin-top-30">
                                <div class="col-md-6">
                                    <a href="{{ route('password.request') }}" class="lostpass">Forgot Password</a>
                                </div>
                                <div class="col-6 text-center">
                                    <a href="{{ route('register') }}" class="loginwith">Create an Account</a>
                                </div>
                            </div>


                        </form>
                    </div>
                </div>
            </div>
        </div>

    </section> -->
    <div id="page-wrapper">
      <section id="content">
        <div class="container">
            <div id="main">

                <div class="text-center yellow-color box" style="font-size: 4em; font-weight: 300; line-height: 1em;">Login!</div>
                <p class="light-blue-color block" style="font-size: 1.3333em;">Please login to your account.</p>
                <div class="col-sm-8 col-md-6 col-lg-5 no-float no-padding center-block">
                  @if (session('logout'))
                      <div class="alert alert-danger alert-dismissible">
                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          {{ session('logout') }}
                      </div>
                  @endif
                  @if (session('success'))
                      <div class="alert alert-success alert-dismissible">
                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          {{ session('success') }}
                      </div>
                  @endif
                  @if (session('danger'))
                      <div class="alert alert-danger alert-dismissible">
                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          {{ session('danger') }}
                      </div>
                  @endif

                    <form class="login-form" id="c-form" action="{{route('login')}}" method="post">
                          @csrf
                        <div class="form-group">
                            <input type="text" name="username" value="{{old('username')}}" class="from-control input-text input-large full-width" placeholder="enter your email or username">
                            @if ($errors->has('username'))
                                <strong class="error" style="color:#cc3300;">{{ $errors->first('username') }}</strong>
                            @endif
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" class="from-control input-text input-large full-width" placeholder="enter your password">
                            @if ($errors->has('password'))
                                <strong class="error" style="color:#cc3300;">{{ $errors->first('password') }}</strong>
                            @endif
                        </div>
                        <div class="form-group">
                          <div class="row">
                            <div class="col-md-8">
                              <label class="checkbox">
                                  <input type="checkbox" value=""> <a href="{{ route('password.request') }}" class="lostpass">Forgot Password</a>
                              </label>
                            </div>

                            <div class="col-md-4">
                              <label class="checkbox">
                                  <input type="checkbox" value=""><a href="{{ route('register') }}" class="loginwith">Create an Account</a>
                              </label>
                            </div>

                          </div>

                        </div>

                        <button type="submit" class="btn-large full-width sky-blue1">LOGIN TO YOUR ACCOUNT</button>
                    </form>
                </div>
            </div>
        </div>
      </section>

      <footer id="footer">
              <div class="footer-wrapper">
                  <div class="container">
                      <nav id="main-menu" role="navigation" class="inline-block hidden-mobile">
                          <ul class="menu">
                              <li class="menu-item-has-children">
                                  <a href="{{route('homepage')}}">Home</a>

                              </li>

                              <li class="menu-item-has-children">
                                  <a href="{{route('about')}}">About</a>

                              </li>
                              <li class="menu-item-has-children">
                                  <a href="car-index.html">Blog</a>

                              </li>
                              <li class="menu-item-has-children">
                                  <a href="cruise-index.html">Faq</a>

                              </li>
                              <li class="menu-item-has-children">
                                  <a href="tour-index.html">Contact</a>

                              </li>

                              <li class="menu-item-has-children">
                                  <a href="#">Booking</a>
                                  <ul>
                                      <li><a href="shortcode-tabs.html">Ticket Cancel</a></li>
                                      <li><a href="shortcode-buttons.html">Print Download</a></li>

                                  </ul>
                              </li>
                              <li class="menu-item-has-children">
                                  <a href="#">Language</a>
                                  <ul>
                                      <li><a href="dashboard1.html">English</a></li>
                                  </ul>
                              </li>
                              <li class="menu-item-has-children">
                                  <a href="hotel-index.html" class="">Buy Ticket</a>
                              </li>
                              <li class="menu-item-has-children">
                                  <a href="{{route('login')}}" class="" >Sign In</a>

                              </li>
                          </ul>
                      </nav>
                      <div class="copyright">
                          <p>© 2014 Travelo</p>
                      </div>
                  </div>
              </div>
          </footer>
    </div>

    <!--================================
        contact us part end
    =====================================-->

@endsection
