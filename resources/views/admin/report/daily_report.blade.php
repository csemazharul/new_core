<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 18-Dec-19
 * Time: 12:51 PM
 */
?>

@extends('admin.layout.master')

@section('body')
    <div>
        {{--        <a class="btn btn-sm  btn-primary pull-right" href="{{ route(Route::current()->getName()) == $full_url ? route(Route::current()->getName())."?" : $full_url.'&' }}type=print" id="btn-print"> <i class="fa fa-print"></i> Print </a>--}}
        <a class="btn btn-sm  btn-primary pull-right" href="{{route('admin_daily_report',['id'=>$trip_asign->id_no,'type'=>'print'])}}" id="btn-print"> <i class="fa fa-print"></i> Print </a>
    </div><br>
    <div class="card">
        <div class="card-header">
            <div style="width: 100%;">
                <div class="row">
                    <div class="col-md-12" >
                        <h2 class="text-center" style="margin-bottom: 5px;"> {{$trip_asign->fleetRegistration->company}}</h2>
                    </div>

                </div>
            </div>
        </div>

        <div class="card-body">

            <table class="table table-bordered">
                <tbody>
                <tr>
                    <th>Trip No</th><td>{{$trip_asign->id_no}}</td>
                    <th>Driver</th><td>{{$driveInfo['driver_name']}}</td>
                    <th>Guider</th><td>{{$driveInfo['superviser_name']}}</td>
                </tr>
                </tbody>
            </table>

            <table class="table table-bordered">
                <tr><th>Coach No</th><td>{{$trip_asign->coach_no}}</td>
                    <th>Starting Place</th><td>{{$trip_asign->start_point}}</td>
                    <th>Journey by date</th><td>{{date('d M Y h:i A',strtotime($trip_asign->start_date))}}</td></tr>
                <tr><th>Route Name</th><td>{{$trip_asign->tripRoute->name}}</td><th>Total Sold</th><td>{{$sold_ticket}}</td><th>Total Available</th><td>{{$trip_asign->fleetRegistration->total_seat - $sold_ticket}}</td></tr>
            </table>


            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Sl No</th>
                    <th>Seats</th>
                    <th>Pass. Name</th>
                    <th>Mobile</th>
                    <th>Ticket No</th>
                    <th>Issue By</th>
                    <th>Issue Counter</th>
                    <th>Total</th>

                    <!-- <th>Commission</th> -->
                </tr>
                </thead>
                <tbody>
                @php
                    $total_fare = 0;
                @endphp

                @foreach($ticket_sale as $k=>$data)
                    <tr>
                        <td>{{++$k}}</td>
                        <td>{{$data->seat_number}}</td>
                        <td>{{$data->passenger_name	}}</td>
                        <td>{{$data->phone}}</td>
                        <td>{{$data->pnr}}</td>

                        <td>{{$data->agent->username}}</td>
                        <td>@if(isset($data->counter->counter_name)){{$data->counter->counter_name}}@endif</td>
                        <td>{{$data->total_fare}}</td>
                    </tr>
                    @php
                        $total_fare += $data->total_fare;
                    @endphp
                    @if($loop->last)
                        <tr>
                            <th colspan="7"> Total </th>
                            <td>{{ create_money_format($total_fare) }}</td>
                        </tr>
                    @endif
                @endforeach


                </tbody>
            </table>



        </div>


        <!-- <div style="width: 100%;">

        </div> -->
    </div>

    <div class="card">
        <div class="card-header">
          <h4 class="text-center">Counter Ticket Sell</h4>
        </div>
        <div class="card-body">
          <table class="table table-bordered">
              <thead>
              <tr>
                  <th>Sl No</th>
                  <th>Counter Name</th>
                  <th>Total Seat Sold</th>
                  <th>Total Price</th>

                  <!-- <th>Commission</th> -->
              </tr>
              </thead>
              <tbody>
                @php
                    $total_fare = 0;
                @endphp

              @foreach($counter_sale as $k=>$data)
                <tr>
                  <td>{{++$k}}</td>
                  <td>@if(isset($data->counter->counter_name)){{$data->counter->counter_name}}@endif</td>
                  <td>{{$data->total_seat	}}</td>
                  <td>{{$data->total_fare}}</td>
                </tr>
                @php
                    $total_fare += $data->total_fare;
                @endphp
                @if($loop->last)
                    <tr>
                        <th colspan="3"> Total </th>
                        <td>{{ create_money_format($total_fare) }}</td>
                    </tr>
                @endif
              @endforeach


              </tbody>
          </table>
        </div>
    </div>

@endsection
@section('script')

@endsection
