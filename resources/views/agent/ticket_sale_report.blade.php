@extends('agent.layout.master')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/jquery.autocomplete.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/flatpickr.min.css')}}">
    {{--<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">--}}
@stop

@section('body')
    <h2 class="mb-4">{{$page_title}}</h2>

    <div class="card">

        <div class="card-body">

            <form role="form" method="get" action="{{ route('report.agent_ticket_sale') }}">

                <div class="row">

                    <div class="col-md-3">
                        <h5>Bus Number</h5>
                        <div class="input-group">
                            <select class="select2 form-control" name="bus_tract_id">
                                <option></option>
                                @foreach($all_bus_reg as $key=>$value)
                                    <option value="{{$value->id_no}}">{{$value->reg_no}}-{{$value->id_no}}-{{$value->name}}</option>
                                @endforeach
                            </select>
                        </div>

                    </div>
                    <div class="col-md-3">
                        <h5> Date</h5>
                        <div class="input-group">
                            <input type="text" class="form-control datepicker" value="" name="date" autocomplete="off">
                        </div>
                    </div>


                </div>
                <br>

                <div class="row form-group">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-success">Search</button>
                    </div>
                </div>

            </form>

        </div>
    <!-- <div style="width: 100%;">
        <table style="margin-bottom: 10px; width: 100%;  margin-top: 5mm;">
            <tr>
                <td style="text-align: center; border: 0 !important;" width="100%" >

                    <h3> Agent: {{ Auth::guard('agent')->user()->username }}</h3>
                </td>
            </tr>

        </table>
    </div> -->


        <div class="card-body">
            <div>
                <a class="btn btn-sm  btn-primary pull-right" href="{{ route(Route::current()->getName()) == $full_url ? route(Route::current()->getName())."?" : $full_url.'&' }}type=print" id="btn-print"> <i class="fa fa-print"></i> Print </a>
            </div>
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>SL</th>
                    <th>Passenger Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Seat No</th>
                    <th>Balance</th>

                    <!-- <th>Commission</th> -->
                </tr>
                </thead>
                <tbody>
                @php
                    $total_fare = 0;

                @endphp

                @foreach($ticket_sales as $k=>$data)
                    <tr>
                        <td>{{ $k+1 }}</td>
                        <td>{{ $data->passenger_name }}</td>
                        <td>{{ $data->email }}</td>
                        <td>{{ $data->phone }}</td>
                        <td>{{ $data->seat_number }}</td>
                        <td>{{ $data->total_fare }}</td>
                    </tr>
                    @php
                        $total_fare += $data->total_fare;

                    @endphp
                    @if($loop->last)
                        <tr>
                            <th colspan="5"> Total </th>
                            <td>{{ create_money_format($total_fare) }}</td>
                        </tr>
                    @endif
                @endforeach

                </tbody>
            </table>
            @if(isset($bus_reg))

            <table class="table table-hover">
                <thead>

                <tr>
                    <th>Agent Name</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th>{{ Auth::guard('agent')->user()->username}}</th>
                </tr>

                <tr>
                    <th>Bus Number</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th>{{$bus_reg}}-{{$bus_tract_id}}</th>
                </tr>

                </thead>

            </table>
                @endif


        </div>
    </div>

@endsection

@section('script')


    <script src="{{ asset('public/assets/jquery.printPage.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.select2').select2();
            $( ".datepicker" ).datepicker();

            $('#btn-print, .btn-print').printPage();
        });
    </script>

@endsection
