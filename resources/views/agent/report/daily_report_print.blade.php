<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 18-Dec-19
 * Time: 3:23 PM
 */

?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    

    <title>  {{ $page_title }}  </title>
    <style>
        * { margin: 0; padding: 0; }
        body {
            font: 14px/1.4 Helvetica, Arial, sans-serif;
        }
        #page-wrap { width: 820px; margin: 0 auto; }

        table {
            display: table;
            border-collapse: collapse;
            border-spacing: 0;
            color: #0a0a0a !important;
            width: 100% !important;
        }

        .table tbody tr td, .table thead tr th, .table tbody tr th, .table thead tr td {
            padding: 3px; !important;
            border: 0.3px solid rgba(1, 1, 1, 0.74) !important;
        }
        .table-border-padding{
            border: 0.3px solid rgba(1, 1, 1, 0.74) !important;
            padding: 3px; !important;
        }
        .text-center{
            text-align: center !important;
        }

        .text-right{
            text-align: right !important;
        }
        .row{
            width:100%px;
        }
        .col1{
            float:left;
            width:70%;
        }
        th td{
            font-size:10px;
        }
  
    .tab1 th{
        width:70px;
        font-size: 12px;
        text-align: center;
       
    }
.tab1 td{
     width:70px;
        font-size: 12px;
        text-align: center;
}
 

        #logo { text-align: right; width: 70px; height: 50px; overflow: hidden; }


        @media print
        {
            .btn-print
            {
                display: none !important;
            }
        }
    </style>
</head>
<body>
    <button style="padding:10px;widht:80px;margin-top:5px;margin-left:5px" class="btn btn-sm btn-success btn-lg btn-print" onClick="window.print();"> <i class="fa fa-print"></i> Print</button>

<div id="page-wrap">

    <div style="overflow: hidden; clear: both;">

        <div class="row">
            <div class="col1" >
                <h2 style="margin-top:10px;text-align:center"> {{$trip_asign->fleetRegistration->company}}</h2>
            </div>

            <div class="col2">
                <h5 style="margin-top: 10px;">Printed By : @if(isset(Auth::guard('agent')->user()->counter->counter_name)){{Auth::guard('agent')->user()->counter->counter_name}}@endif</h5>
                <h5 style="margin-top: 10px;">Date & Time : {{date('d M Y h:i A')}} </h5>
            </div>
        </div><br>

        <table cellspacing="0" width="100%" class="table tab1">
            <tbody>
            <th>Trip No</th><td>{{$trip_asign->id_no}}</td>
            <th>Driver</th><td>{{$driveInfo['driver_name']}}</td>
            <th>Guider</th><td>{{$driveInfo['superviser_name']}}</td>

            </tbody>
        </table>

        <table cellspacing="0" width="100%" class="table tab1">
            <tr><th>Coach No</th><td>{{$trip_asign->coach_no}}</td>
                <th>Starting Place</th><td>{{$trip_asign->start_point}}</td>
                <th>Journey by date</th><td>{{date('d M Y h:i A',strtotime($trip_asign->start_date))}}</td></tr>
            <tr><th>Route Name</th><td>{{$trip_asign->tripRoute->name}}</td><th>Total Sold</th><td>{{$sold_ticket}}</td><th>Total Available</th><td>{{$trip_asign->fleetRegistration->total_seat - $sold_ticket}}</td></tr>
        </table>

        <table cellspacing="0" width="100%" class="table tab1">
            <thead>
            <tr>
                <th>Sl No</th>
                <th>Seats</th>
                <th>Pass. Name</th>
                <th>Mobile</th>
                <th>Ticket No</th>
                <th>Issue By</th>
                <th>Issue Counter</th>
                <th>Total</th>

            </tr>
            </thead>
            <tbody>
            @php
                $total_fare = 0;
            @endphp

            @foreach($ticket_sale as $k=>$data)
                <tr>
                    <td>{{++$k}}</td>
                    <td>{{$data->seat_number}}</td>
                    <td>{{$data->passenger_name	}}</td>
                    <td>{{$data->phone}}</td>
                    <td>{{$data->pnr}}</td>

                    <td>{{$data->agent->username}}</td>
                    <td>@if(isset($data->counter->counter_name)){{$data->counter->counter_name}}@endif</td>
                    <td>{{$data->total_fare}}</td>
                </tr>
                @php
                    $total_fare += $data->total_fare;
                @endphp
                @if($loop->last)
                    <tr>
                        <th colspan="7"> Total </th>
                        <td>{{ create_money_format($total_fare) }}</td>
                    </tr>
                @endif
            @endforeach


            </tbody>
        </table><br>

       <div class="card-header">
              <h4 style="text-align:center">Counter Ticket Sell</h4><br>
        </div>

        <table cellspacing="0" width="100%" class="table tab1">
                  <thead>
                  <tr>
                      <th>Sl No</th>
                      <th>Counter Name</th>
                      <th>Total Seat Sold</th>
                      <th>Total Price</th>

                      <!-- <th>Commission</th> -->
                  </tr>
                  </thead>
                  <tbody>
                    @php
                        $total_fare = 0;
                    @endphp

                  @foreach($counter_sale as $k=>$data)
                    <tr>
                      <td>{{++$k}}</td>
                      <td>@if(isset($data->counter->counter_name)){{$data->counter->counter_name}}@endif</td>
                      <td>{{$data->total_seat	}}</td>
                      <td>{{$data->total_fare}}</td>
                    </tr>
                    @php
                        $total_fare += $data->total_fare;
                    @endphp
                    @if($loop->last)
                        <tr>
                            <th colspan="3"> Total </th>
                            <td>{{ create_money_format($total_fare) }}</td>
                        </tr>
                    @endif
                  @endforeach


                  </tbody>
              </table>


    </div>
    
      <div class="ticket-footer" style="text-align: center;margin-top: 0px;"><br>
        <p style="font-size:12px;font-weight:normal;"><b>Software Developed and Maintaince By : Rcreation || 01722964303,01813316786</b> </p><br>

    </div>


</div>
</body>
</html>
