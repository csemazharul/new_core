@extends('agent.layout.master')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/jquery.autocomplete.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/flatpickr.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/admin-seat-custom.css')}}">
    {{--<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">--}}
    <style>
        .collapse1{
            display: none;
            min-height: 400px;
            max-height: 400px;

        }
    </style>
@stop

@section('body')
    <h4 class="mb-4"><i class="fas fa-bus"></i> {{$page_title}} </h4>

    <div class="card">
        <div class="card-header bg-white font-weight-bold">
            <form action="{{route('agent.tripSearch')}}" method="get">
                <div class="form-row">
                    <div class="col-lg-3">
                        <input type="text" name="start_point" value="{{old('start_point')}}" class="form-control form-control-lg" id="fromAutoComplete" placeholder="From">
                    </div>
                    <div class="col-lg-3">
                        <input type="text" name="end_point"  value="{{old('end_point')}}" id="toAutoComplete" class="form-control form-control-lg" placeholder="To">
                    </div>
                    <div class="col-lg-3">
                        <input type="text" name="date" id="datetimepicker2"  value="{{old('date')}}" class="form-control form-control-lg" placeholder="Date">
                    </div>

                    <div class="col-lg-3">
                        <button type="submit" class="btn btn-success btn-lg  h-serch mamunur_rashid_form_sm">
                           <i class="fa fa-search"></i> SEARCH
                        </button>
                    </div>
                </div>
            </form>
        </div>



        <div class="card-body">

            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Operator</th>
                    <th>Departure</th>
                    <th>Duration</th>
                    <th>Distance</th>
                    <th>Arrival</th>
                    <th>Total Seat</th>
                    <th>Fare</th>
                    <th>View Seats</th>
                </tr>
                </thead>

                <tbody>
                @if(count($checkAssignTrip)>0)
                    @foreach($checkAssignTrip as $key=>$data)
                        <tr>
                            <td>
                                <div class="t-box-1">
                                    <h5>{{$data->tripRoute->name}}</h5>
                                    <strong>{{date('d M Y',strtotime($data->start_date))}}</strong>
                                </div>
                            </td>
                            <td>
                                <h5>{{date('h:s A',strtotime($data->start_date))}}</h5>
                                <strong class="text-success">{{$data->tripRoute->start_point_name}}</strong>
                            </td>
                            <td>
                                <div class="media">
                                    <div class="media-body">
                                        <strong class="text-danger">{{($data->tripRoute->approximate_time) ?? '-'}}</strong>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <strong>{{$data->distance}}</strong>
                            </td>
                            <td>
                                <strong class="text-success">{{$data->tripRoute->end_point_name}}</strong>
                            </td>

                            <td>
                                <div class="p-img">
                                    <strong>{{$data->fleetRegistration->total_seat}} seats</strong>
                                </div>
                                <b class="text-success">{{$data->fleetRegistration->fleetType->name}}</b>
                            </td>
                            @php
                                $ticketPrice =  \App\TicketPrice::where('trip_route_id',$data->trip_route_id)->where('fleet_type_id',$data->fleetRegistration->fleet_type_id)->latest()->first();
                            @endphp


                            <td>

                                <div class="p-img">
                                    @if($ticketPrice)
                                        <strong>{{($ticketPrice->price) ?? '' }} {{$basic->currency}}</strong>

                                    @else
                                        <strong class="text-danger">-</strong>
                                    @endif
                                </div>
                            </td>

                            <td>
                                <div class="l-box">
                                    <div class="media">
                                        <div class="media-body align-self-end">
                                            <div class="link">

                                                <button title="View Seats" value="{{$data->id}}" data-href="#content_{{$key}}" data-id="{{$key}}" class="seat btn btn-icon btn-pill btn-primary">
                                                    <i class="fa fa-eye"></i>
                                                </button>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>

                        </tr>
                        <tr class="content1 collapse1" id="content_{{$key}}">

                           <td colspan="8">
                               <div class="row">
                                 <div class="col-6" id="seat_plan_{{$key}}">

                                 </div>
                                 <div class="col-6" id="pasengerInfo">

                                 </div>
                               </div>
                           </td>
                        </tr>


                    @endforeach

                @else

                    <tr>
                        <td colspan="8">
                            <h4 class="text-center text-danger margin-top-40 margin-bottom-60">No result found!!</h4>
                        </td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>

@endsection
@section('script')
    <script src="{{asset('assets/front/js/jquery.autocomplete.js')}}"></script>
    <script src="{{asset('assets/front/js/flatpickr.js')}}"></script>

    <script type="text/javascript">
    $(document).ready(function() {
    $("button").click(function() {
      var id=this.value;
      var contentId =this.dataset.href;
      var data_id = this.dataset.id;

      $(contentId).toggleClass("collapse1");
      $("#seat_plan_"+data_id).empty();
      $.ajax({
           type: "get",
           url:'view-seat/'+id,
            data: {id: id},
            success: function (data) {
              var rowSeat = 1;
               var totalSeats = 1;
               var seatData = {};
                 console.log(data);
               if(data.seatArray.length>=3)
               {
                   var lastSeats = data.seatArray.length-5;
               }
               else {
                   var lastSeats = data.seatArray.length;
               }
               if(data.seatlayout == '2-2'){
                  data.seatArray.pop();
                  for(let i=0;i<=data.seatArray.length;i++)
                  {
                    if (rowSeat == 1){
                     seatData['seats'] = $("#seat_plan_"+data_id).append("<div class='row'></div>");
                     }
                      seatData['seats'] += '<div class="col-2"><div class='+($.inArray(data.seatArray[i], data.bookArray)?("seat ladies"):("seat occupied ChooseSeat"))+((data.seatArray[i] == 'M') ?  (" last-seat-32 "):(" "))+'><div class="seat-body">'+data.seatArray[i]+'<span class="seat-handle-left"></span><span class="seat-handle-right"></span> <span class="seat-bottom"></span></div></div></div>';
                      //let array_value = $.inArray( data.seatArray[i], data.bookArray );
                //  $(".removeclass"+id).prepend('<div class="alert alert-danger" id="invalid'+id+'"><b>Invalid File Type. Allow file type ("JPG","PNG","JPEG")</b></div>');
                    //  console.log(data.bookArray);
                      $("#seat_plan_"+data_id).append(seatData['seats']);
                     //console.log(seatData['seats']);
                  }

                }
            },
            error: function (res) {
                //console.log(res);
            }
       });


    });

});
        // var coll = document.getElementsByClassName("seat");
        // var i;
        // for (i = 0; i < coll.length; i++) {
        //     coll[i].addEventListener("click", function(e) {
        //         e.preventDefault();
        //         let id=this.value;
        //         var data_id = this.getAttribute("data-id");
        //
        //         this.classList.toggle("active");
        //         let parentTr = this.closest("tr");
        //         console.log(parentTr);
        //         let classchek = parentTr.nextElementSibling;
        //         if(classchek)
        //         {
        //             if(classchek.classList.contains("content1"))
        //             {
        //
        //             }
        //             else
        //             {
        //                 var trNoteAdd = document.createElement('div');
        //                 parentTr.after(trNoteAdd);
        //                 var content = trNoteAdd.classList.add("content1");
        //                 var t = document.createTextNode("Hello World");
        //                 trNoteAdd.appendChild(t);
        //             }
        //         }
        //         else
        //         {
        //             var trNoteAdd = document.createElement('div');
        //             parentTr.after(trNoteAdd);
        //             var content = trNoteAdd.classList.add("content1");
        //             var t = document.createTextNode("Hello World");
        //             trNoteAdd.appendChild(t);
        //         }
        //
        //         if (document.getElementsByClassName("content1")[data_id].style.display === "block"){
        //
        //           document.getElementsByClassName("content1")[data_id].style.display = "none";
        //         } else {
        //             document.getElementsByClassName("content1")[data_id].style.display = "block";
        //         }
        //     });
        // }

        $(document).ready(function () {
            var states = {!! $tripFrom !!};
            $(function () {
                $("#fromAutoComplete").autocomplete({
                    source: [states]
                });
            });
            $(function () {
                $("#toAutoComplete").autocomplete({
                    source: [states]
                });
            });

            $("#datetimepicker2").flatpickr({
                minDate: "today",
                maxDate: new Date().fp_incr(50), // 14 days from now
                dateFormat: "d M Y",
            });


        });
    </script>
@stop
