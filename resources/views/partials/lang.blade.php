
<li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle
                            " href="#" id="navbarDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true"
       aria-expanded="false">@lang('Language')
    </a>
    <div class="dropdown-menu" aria-labelledby="navbarDropdown2">
            <a class="dropdown-item" href="{{route('lang', 'en') }}">@lang('English')</a>
        @foreach($lan as $data)
            <a class="dropdown-item" href="{{ route('lang', $data->code) }}">{{__($data->name)}}</a>
        @endforeach
    </div>
</li>