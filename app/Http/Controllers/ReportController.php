<?php

namespace App\Http\Controllers;

use App\Agent;
use App\Counter;
use App\FleetRegistration;
use App\FleetType;
use App\RideInfo;
use App\TicketBooking;
use App\TripAssign;
use App\TripLocation;
use App\TripRoute;
use Carbon\Carbon;
use Auth;
use http\Env\Response;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;


class ReportController extends Controller
{
    public function ticket_sale(Request $request){

        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $agent_id = $request->point_id;
        $trip_route_id = $request->route_id;
        $ticket_sales = new TicketBooking();

        if(!empty($agent_id))
            $ticket_sales = $ticket_sales->where('agent_id', $agent_id);

        if(!empty($trip_route_id))
            $ticket_sales = $ticket_sales->where('trip_route_id', $trip_route_id);

        if( !empty($start_date) )
            $start_date = db_date_format($start_date);

        if( !empty($end_date) )
            $end_date = db_date_format($end_date);


        if( empty($start_date) && (!empty($end_date)) ) {

            $ticket_sales = $ticket_sales->whereDate('booking_date','<=', $end_date);
        }elseif( (!empty($start_date)) && empty($end_date) ) {

            $ticket_sales = $ticket_sales->whereDate('booking_date','>=', $start_date);
        }elseif ( $start_date !='' && $end_date != '' ) {

            $ticket_sales = $ticket_sales->whereBetween('booking_date',[$start_date, $end_date]);
        }



        $data['full_url'] =  $request->fullUrl().($request->fullUrl() == $request->url() ? "?" : "&");
        $data['trip_routes'] = TripRoute::get()->pluck('name','id');
        $data['agents'] = Agent::get()->pluck( 'full_name','id');
        $data['page_title'] = "Ticker Sale Report";

        if($request->type=="print" || $request->type=="download") {

            $data['ticket_sales'] = $ticket_sales->where('payment_status',1)->where('cancel_req',0)->get();
            if ($request->type == "print") {
                return view('admin.report.print_ticket_sale', $data);
//            } else if ($request->type == "download") {
//                $pdf = PDF::loadView('member.reports.print_daily_stock_report', $data);
//                $file_name = file_name_generator($title);
//                return $pdf->download($file_name);
            }
        } else {

            $data['ticket_sales'] = $ticket_sales->where('payment_status',1)->where('cancel_req',0)->paginate(15);
            return view('admin.report.ticket_sales',$data);
        }

    }

    public function agentTicketSale(Request $request)
    {
       $request->print;

        $from_date = "";
        $to_date = "";
        $today = db_date_format(Carbon::now());
        $agent_id =  Auth::guard('agent')->user()->id;


        $view['page_title'] = "Trip Search";
        $ticket_sales = TicketBooking::select('id_no',DB::raw('SUM(total_seat) as total_seat'), DB::raw('SUM(total_fare) as total_fare'), DB::raw('SUM(discount) as total_discount'),DB::raw('SUM(cross_total) as cross_total'))->groupBy('id_no')->where('payment_status',1)->where('cancel_req',0)->where('agent_id',$agent_id);

        $data['all_coach'] = TripAssign::where('status',1)->where('coach_no','!=','')->orderBy('coach_no', 'dsc')->distinct()->pluck('id_no','coach_no');

        if(!empty($request->from_date))
        {
            $from_date = db_date_format($request->from_date);
        }
        if(!empty($request->to_date))
        {
            $to_date = db_date_format($request->to_date);
        }

        if(empty($from_date) && !empty($to_date) ) {

            $ticket_sales = $ticket_sales->whereDate('booking_date','>=', $to_date);

        }

        if(!empty($from_date) && empty($to_date) ) {
        $ticket_sales = $ticket_sales->whereDate('booking_date','>=', $from_date);
        }

        if( $from_date !='' && $to_date != '' ) {
            $ticket_sales = $ticket_sales->whereBetween('booking_date',[$from_date, $to_date]);

        }
        if( $from_date =='' && $to_date == '' )
        {
            $ticket_sales = $ticket_sales->whereDate('booking_date',$today);
        }
        $data['page_title'] = "Agent Sale Report";
        $data['ticket_sales'] = $ticket_sales->get();

        $data['full_url'] =  $request->fullUrl().($request->fullUrl() == $request->url() ? "?" : "&");

            if ($request->type == 'print') {
              return view('agent.report.agent_report_print',$data);
            }
        else {
            return view('agent.report.agent_report',$data);
        }

    }


    public function tripFindAgent(Request $request)
    {

      $from_date = "";
      $to_date = "";
      $today = db_date_format(Carbon::now());
      $from_location = $request->from_location;
      $to_location = $request->to_location;
      $trip_id = $request->trip_id;
      $coach_no = $request->coach_no;

      $view['page_title'] = "Daily Report";
      $trip_asign = TripAssign::
                      join('trip_routes', 'trip_routes.id', '=', 'trip_assigns.trip_route_id')
                      ->select('trip_assigns.*', 'trip_routes.start_point_name as start_point_name', 'trip_routes.end_point_name as end_point_name', 'trip_routes.stoppage as stoppage', 'trip_routes.distance as distance', 'trip_routes.approximate_time as approximate_time', 'trip_routes.name as name');


      $from = TripLocation::where('status', 1)->orderBy('name', 'asc')->get();
      $all_coach = TripAssign::where('status',1)->where('coach_no','!=','')->orderBy('coach_no', 'dsc')->distinct()->pluck('id_no','coach_no');

      $tripFrom = [];
      foreach ($from as $value) {
            $tripFrom[] = $value->name;
        }
      $fleetType = FleetType::where('status', 1)->orderBy('name', 'asc')->get();
      $type = [];
      foreach ($fleetType as $value) {
            $type[] = $value->name;
        }
      $tripFrom = json_encode($tripFrom, true);
      $type = json_encode($type, true);
        if(!empty($request->from_date))
        {
          $from_date = db_date_format($request->from_date);
        }
        if(!empty($request->to_date))
        {
          $to_date = db_date_format($request->to_date);
        }
        if(empty($from_date) && !empty($to_date) ) {
            $trip_asign = $trip_asign->whereDate('start_date','<=', $to_date);
        }else if(!empty($from_date) && empty($to_date) ) {
            $trip_asign = $trip_asign->whereDate('start_date','>=', $from_date);
        }else if( $from_date !='' && $to_date != '' ) {
          //$trip_asign = $trip_asign->whereBetween('start_date',[$from_date, $to_date]);
          $trip_asign = $trip_asign->whereRaw('date(`start_date`) between "' . $from_date. '" and "' .$to_date . '"');
        }
        if(empty($from_location) && !empty($to_location))
        {
            $trip_asign = $trip_asign->where('end_point_name',$to_location);
        }
        if ($from_location !='' && $to_location != '')
        {
            $trip_asign = $trip_asign->where('start_point_name', $from_location)->where('end_point_name',$to_location);
        }
        if(!empty($coach_no))
        {
            $trip_asign = $trip_asign->where('coach_no', $coach_no);
        }

        if(!empty($trip_id))
        {

            $trip_asign = $trip_asign->where('id_no', $trip_id);
        }

         if($from_date =='' && $to_date == '' &&  $coach_no=='' && $from_location =='' && $to_location=='') {
            $trip_asign = $trip_asign->whereDate('start_date',$today);
        }
        $trip_asign = $trip_asign->get();

        return view('agent.trip_view',$view, compact('tripFrom','all_coach','type','trip_asign',  'startDate'));

    }

   public function agentDailyReport($id,$type)
   {

     $data['trip_asign'] = TripAssign::where('id_no',$id)->first();
     $fleet_registration_id = $data['trip_asign']['fleetRegistration']['id'];
     $data['driveInfo'] = RideInfo::where('bus_id',$fleet_registration_id)->first();

     $data['ticket_sale'] = TicketBooking::where('id_no',$id)
                           ->where('payment_status',1)
                           ->where('cancel_req',0)
                           ->get();
     $data['sold_ticket'] = TicketBooking::where('id_no',$id)->sum('total_seat');
     $data['counter_sale'] = TicketBooking::select('id_no','fleet_registration_id','counter_id', DB::raw('SUM(total_seat) as total_seat'), DB::raw('SUM(total_fare) as total_fare'))
                          ->groupBy('counter_id')
                          ->where('payment_status',1)
                          ->where('id_no',$id)
                          ->where('cancel_req',0)
                          ->get();

     $data['page_title'] = "Daily Report";
     if($type=='view')
         return view('agent.report.daily_report',$data);
     else
        return view('agent.report.daily_report_print',$data);

   }

    public function tripFindAdmin(Request $request)
    {


        $from_date = "";
        $to_date = "";
        $today = db_date_format(Carbon::now());

        $agent_id = $request->agent_id;
        $counter_id = $request->counter_id;
        $trip_id = $request->trip_id;

        $data['page_title'] = "Ticket Sale Report";
        $ticket_sales = TicketBooking::select('id_no','booking_date',DB::raw('SUM(total_seat) as total_seat'), DB::raw('SUM(total_fare) as total_fare'), DB::raw('SUM(discount) as total_discount'),DB::raw('SUM(cross_total) as cross_total'))->groupBy('id_no')->where('payment_status',1)->where('cancel_req',0);

        $data['all_coach'] = TripAssign::where('status',1)->where('coach_no','!=','')->orderBy('coach_no', 'dsc')->distinct()->pluck('id_no','coach_no');
        //return $data['all_coach'];

        $data['agents'] = Agent::where('status',1)->orderBy('username', 'dsc')->pluck('username','id');
        $data['counters'] = Counter::where('status',1)->orderBy('counter_name', 'dsc')->pluck('counter_name','id');

        if(!empty($request->from_date))
        {
            $from_date = db_date_format($request->from_date);
        }
        if(!empty($request->to_date))
        {
            $to_date = db_date_format($request->to_date);
        }
        if(empty($from_date) && !empty($to_date) ) {

        $ticket_sales = $ticket_sales->whereDate('booking_date','<=', $to_date);

        }
        else if(!empty($from_date) && empty($to_date) ) {

        $ticket_sales = $ticket_sales->whereDate('booking_date','>=', $from_date);

        }
        // else if( $from_date == $to_date ) {
        //  $ticket_sales = $ticket_sales->whereDate('booking_date','=',$from_date);
        //
        //  }
       else if( $from_date !='' && $to_date != '' ) {
        $ticket_sales = $ticket_sales->whereBetween('booking_date',[$from_date, $to_date]);

        }
        if(!empty($trip_id))
        {
            $ticket_sales = $ticket_sales->where('id_no', $trip_id);
        }
        if(!empty($counter_id))
        {
            $ticket_sales = $ticket_sales->where('counter_id', $counter_id);
        }
        if(!empty($agent_id))
        {

            $ticket_sales = $ticket_sales->where('agent_id', $agent_id);
        }
         if($from_date =='' && $to_date == '' && $agent_id == '' && $counter_id == '' && $trip_id == '') {

            $ticket_sales = $ticket_sales->whereDate('booking_date',$today);
        }
        $data['ticket_sales'] = $ticket_sales->get();
        $data['full_url'] =  $request->fullUrl().($request->fullUrl() == $request->url() ? "?" : "&");

        if($request->type == 'print')
        {
          return view('admin.report.daily_sale_report',$data);
        }
        else {
          return view('admin.trip.assign.trip_search_view',$data);
        }

    }
    public function admintDailyReport($id,$type)
    {
        $data['trip_asign'] = TripAssign::where('id_no',$id)->first();
        $fleet_registration_id = $data['trip_asign']['fleetRegistration']['id'];
        $data['driveInfo'] = RideInfo::where('bus_id',$fleet_registration_id)->first();

        $data['ticket_sale'] = TicketBooking::where('id_no',$id)
            ->where('payment_status',1)
            ->where('cancel_req',0)
            ->get();
       $data['counter_sale'] = TicketBooking::select('id_no','fleet_registration_id','counter_id', DB::raw('SUM(total_seat) as total_seat'), DB::raw('SUM(total_fare) as total_fare'))
                                 ->groupBy('counter_id')
                                 ->where('payment_status',1)
                                 ->where('id_no',$id)
                                 ->where('cancel_req',0)
                                 ->get();
        $data['sold_ticket'] = TicketBooking::where('id_no',$id)->sum('total_seat');
        $data['page_title'] = "Daily Report";

        if($type=='view')
            return view('admin.report.daily_report',$data);
        else
            return view('admin.report.daily_report_print',$data);
    }



}
