<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;
use Artisan;

class BackupController extends Controller
{
  public function create()
{
  try {

      // start the backup process
      Artisan::call('backup:run',['--only-db'=>true]);
      $output = Artisan::output();
      // log the results
      //Log::info("Backpack\BackupManager -- new backup started from admin interface \r\n" . $output);
      // return the results as a response to the ajax call
      echo "Success";
      return redirect()->back();
  } catch (Exception $e) {
    echo "Fail";
    die;
    //  Flash::error($e->getMessage());

      return redirect()->back();
  }
}

}
