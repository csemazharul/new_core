<?php

namespace App\Http\Controllers;

use App\Agent;
use App\FleetType;
use App\GeneralSettings;
use App\LogPdf;
use App\TicketBooking;
use App\TripAssign;
use App\TripLocation;
use App\Gateway;
use App\Deposit;
use App\Trx;
use DB;



use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Image;
use File;
use Auth;
use Carbon\Carbon;
use Session;

class AgentMasterController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:agent');
    }

    public function dashboard()
    {
        $data['page_title'] = 'DashBoard';
        $data['Gset'] = GeneralSettings::first();
        $user = Auth::guard('agent')->user();
        $data['invests'] = TicketBooking::where('agent_id',$user->id)->where('payment_status',1)->latest()->paginate(15);
        // echo "<pre>";
        // echo json_encode($data);
        // die;
        $data['balance'] = $user->balance;
        $data['soldProfit'] = TicketBooking::where('agent_id',$user->id)->where('payment_status',1)->sum('commission');
        $data['deposit'] = Deposit::where('user_id',$user->id)->whereStatus(1)->where('role',2)->sum('amount');
        return view('agent.dashboard', $data);
    }

    public function agentTripFind()
    {

        $view['page_title'] = "Trip Search";

        $from = TripLocation::where('status', 1)->orderBy('name', 'asc')->get();
        $tripFrom = [];
        foreach ($from as $value) {
            $tripFrom[] = $value->name;
        }
        $fleetType = FleetType::where('status', 1)->orderBy('name', 'asc')->get();
        $type = [];
        foreach ($fleetType as $value) {
            $type[] = $value->name;
        }
        $tripFrom = json_encode($tripFrom, true);
        $type = json_encode($type, true);

        $startDate = date('Y-m-d');

        $checkAssignTrip = TripAssign::
        join('trip_routes', 'trip_routes.id', '=', 'trip_assigns.trip_route_id')
            ->select('trip_assigns.*', 'trip_routes.start_point_name as start_point_name', 'trip_routes.end_point_name as end_point_name', 'trip_routes.stoppage as stoppage', 'trip_routes.distance as distance', 'trip_routes.approximate_time as approximate_time', 'trip_routes.name as name')
            ->when($startDate, function ($query) use ($startDate) {
                return $query->whereDate('start_date', '=', $startDate);
            })
            ->get();

        return view('agent.trip-find',$view, compact('tripFrom','type','checkAssignTrip',  'startDate'));
    }

    public function agentTripSearch(Request $request)
    {

        $view['page_title'] = "Trip Search";
        $from = TripLocation::where('status', 1)->orderBy('name', 'asc')->get();
        $tripFrom = [];
        foreach ($from as $value) {
            $tripFrom[] = $value->name;
        }
        $fleetType = FleetType::where('status', 1)->orderBy('name', 'asc')->get();
        $type = [];
        foreach ($fleetType as $value) {
            $type[] = $value->name;
        }

        $tripFrom = json_encode($tripFrom, true);
        $type = json_encode($type, true);
        if ($request->date) {
            $startDate = date('Y-m-d', strtotime($request->date));
        } else {
            $startDate = date('Y-m-d');
        }
        $start_point = ($request->start_point) ?? null;
        $end_point = ($request->end_point) ?? null;
        $checkAssignTrip = TripAssign::
        join('trip_routes', 'trip_routes.id', '=', 'trip_assigns.trip_route_id')
            ->select('trip_assigns.*', 'trip_routes.start_point_name as start_point_name', 'trip_routes.end_point_name as end_point_name', 'trip_routes.stoppage as stoppage', 'trip_routes.distance as distance', 'trip_routes.approximate_time as approximate_time', 'trip_routes.name as name')
            ->when($request->start_point, function ($query) use ($request) {
                return $query->where('trip_routes.start_point_name', 'like', '%' . $request->start_point . '%');
            })
            ->when($request->end_point, function ($query) use ($request) {
                return $query->where('trip_routes.end_point_name', 'like', '%' . $request->end_point . '%');
            })
            ->when($startDate, function ($query) use ($startDate) {
                return $query->whereDate('start_date', '=', $startDate);
            })
            ->get();

        return view('agent.trip-search',$view, compact('tripFrom','type','checkAssignTrip', 'start_point', 'end_point', 'startDate'));
    }

    public function tripViewAgent($id)
    {

      $data['ticket_sales'] = DB::table('ticket_bookings')
                             ->join('agents', 'ticket_bookings.agent_id', '=', 'agents.id')
                             ->select('ticket_bookings.fleet_registration_id','ticket_bookings.passenger_name','ticket_bookings.email','ticket_bookings.phone','ticket_bookings.seat_number','ticket_bookings.total_fare','agents.username')
                             ->where('payment_status',1)
                             ->where('cancel_req',0)
                             ->where('agent_id', Auth::guard('agent')->user()->id)
                             ->Where('id_no',$id)
                             ->get();

      $data['page_title']="Chalan sheet";
      $data['id']=$id;
      $fleet_registration_id = array_column(json_decode($data['ticket_sales'],true),'fleet_registration_id');
      if($fleet_registration_id)
      {
        $data['driverInfo'] = DB::table('ride_infos')->where('bus_id', $fleet_registration_id[0])->first();
      }

      return view('agent.trip.agent_assing', $data);
    }
    public function tripPrintView($id)
    {

      $data['passengers'] = DB::table('ticket_bookings')
                            ->join('agents', 'ticket_bookings.agent_id', '=', 'agents.id')
                            ->select('ticket_bookings.fleet_registration_id','ticket_bookings.passenger_name','ticket_bookings.email','ticket_bookings.phone','ticket_bookings.seat_number','ticket_bookings.total_fare','agents.username')
                            ->where('payment_status',1)
                            ->where('cancel_req',0)
                            ->where('agent_id', Auth::guard('agent')->user()->id)
                            ->Where('id_no',$id)
                            ->get();
      $data['page_title']="Chalan sheet";
      $fleet_registration_id = array_column(json_decode($data['passengers'],true),'fleet_registration_id');
      if($fleet_registration_id)
      {
        $data['driverInfo'] = DB::table('ride_infos')->where('bus_id', $fleet_registration_id[0])->first();
      }

      return view('agent.ticket_sale_print', $data);
    }

    public function viewSeat($id)
    {

      $tripAssign = TripAssign::where('id', $id)->where('status', 1)->first();
      if ($tripAssign) {
          $data['page_title'] = $tripAssign->tripRoute->name;
          $data['tripAssign'] = $tripAssign;

          $dropLocation = array_map('trim', explode(',', $tripAssign->tripRoute->stoppage));
          $stoppage = [];
          foreach ($dropLocation as $value) {
              $stoppage[] = $value;
          }

           $data['stoppage'] = $stoppage;

          $ticketBook =  TicketBooking::where('trip_assign_id_no',$tripAssign->id)->where('payment_status',1)->where('status','!=',-1)->get();
          $seatArray = "";
          $female = "";
          $male = "";
          foreach ($ticketBook as $val){
              $seatArray .= $val->seat_number ;
          }
          foreach ($ticketBook as $val){
            if($val->gender==0)
            {
              $female.= $val->seat_number;
            }

          }
          foreach ($ticketBook as $val){
            if($val->gender==1)
            {
              $male.= $val->seat_number;
            }

          }
          $data['booked_serial'] = $seatArray;
          $data['female'] = $female;
          $data['male'] = $male;
          //new code started
          $data['bookArray'] = array_map('trim', explode(',', $data['booked_serial']));
          $data['femaleArray'] = array_map('trim', explode(',', $data['female']));
          $data['maleArray'] = array_map('trim', explode(',', $data['male']));

        //  $seatArray = array();
          $seatArray = array_map('trim', explode(',', $tripAssign->fleetRegistration->seat_numbers));
          $data['seatlayout']= $tripAssign->fleetRegistration->layout;
          $data['layoutLastSeat']= $tripAssign->fleetRegistration->lastseat;

         $data['seatArray'] = $seatArray;
         return response()->json($data);



          //return view('agent.seat-plan', $data);

      }
      abort(404);
    }
  public function checkedSeat(Request $request)
    {

       $auth =  Auth::guard('agent')->user();

//        if($auth->balance < $request->total_fare){
////            return response()->json(['arr'=> 'Insufficient Balance' , 'status' => 'insufficient']);
//        }else{

            $ticketBook =  TicketBooking::where('trip_assign_id_no',$request->trip_assign_id_no)->where('status','!=',-1)->get();
            $seatArray = "";
            foreach ($ticketBook as $val){
                $seatArray .= $val->seat_number ;
            }

            $seatBookedArray = array_map('trim', explode(',', $seatArray));
            $seatNumberRequest = array_map('trim', explode(',', $request->seat_number));


            $bookedSeatYet = [];
            foreach ($seatNumberRequest as $reqSeat)
            {
                if (in_array($reqSeat, $seatBookedArray)){
                    $bookedSeatYet[] = $reqSeat;
                }
            }
            array_pop($bookedSeatYet);


            if ($bookedSeatYet != []){
                return response()->json(['arr'=> $bookedSeatYet , 'status' => 1000]);
            }

            $basic = GeneralSettings::first();

            $commission = ($request->total_fare * $basic->agent_com)/100;
            $admin_get = round(($request->total_fare - $commission),$basic->decimal);

             if(Auth::guard('agent')->user()->counter)
            {
              $data['counter_id'] = $user = Auth::guard('agent')->user()->counter->id;
            }
            if(!empty($request->discount))
            {
              $data['discount'] = $request->discount;
              $data['cross_total'] = $request->total_fare-$request->discount;
            }
            else {
                $data['discount'] = 0;
                $data['cross_total'] = $request->total_fare;
            }
            $data['trip_route_id'] = $request->trip_route_id;
            $data['fleet_registration_id'] = $request->fleet_registration_id;
            $data['trip_assign_id_no'] = $request->trip_assign_id_no;
            $data['fleet_type_id'] = $request->fleet_type_id;
            $data['id_no'] = $request->id_no;
            $data['gender'] = $request->gender;
            $data['gender']=0;

            $data['user_id'] =  null;
            $data['boarding'] = ucfirst($request->boarding);
            $data['total_seat'] = $request->total_seat;
            $data['seat_number'] = $request->seat_number;
            $data['price'] = $request->price;
            $data['total_fare'] = $request->total_fare;
            $data['booking_date'] = $request->booking_date;
            $data['passenger_name'] = $request->passenger_name;
            $data['email'] = $request->email;
            $data['phone'] = $phone = phone_number_format($request->phone);
            $data['agent_id'] = $auth->id;
            $data['payment_status'] = 1;
            $data['admin_get'] = $admin_get;
            $data['commission'] = round($commission,$basic->decimal);

            $data['pay_endtime'] = Carbon::now()->addMinutes(2);
            $data['cancel_endtime'] = Carbon::parse($request->booking_date)->subMinutes($basic->cancel_endtime);
            $data['pnr'] = $pnr =  strtoupper(str_random(12));
            $TicketBookId = TicketBooking::create($data)->id;

            $ticketBooking = TicketBooking::where('id',$TicketBookId)->first();

            $auth->balance = round(($auth->balance - $request->total_fare), $basic->decimal);
            $auth->save();


            Trx::create([
                'user_id' => $auth->id,
                'amount' => round($request->total_fare, $basic->decimal),
                'main_amo' => round($auth->balance,$basic->decimal),
                'charge' => 0,
                'role' => 2,
                'type' => '-',
                'title' => $ticketBooking->tripRoute->name .' ( '. date('d M Y h:i A',strtotime($ticketBooking->tripAssign->start_date)) .' )'. " Seats: $ticketBooking->seat_number" ,
                'trx' => 'TRX-'.rand(000000,999999).rand(000000,999999)
            ]);

            $agent  = Agent::find($auth->id);
            $agent->balance = $agent->balance + $commission;
            $agent->save();

            Trx::create([
                'user_id' => $auth->id,
                'amount' => round($commission,$basic->decimal),
                'main_amo' => round($agent->balance,$basic->decimal),
                'charge' => 0,
                'role' => 2,
                'type' => '+',
                'title' => "Commission From ".$ticketBooking->tripRoute->name .' ( '. date('d M Y h:i A',strtotime($ticketBooking->tripAssign->start_date)) .' )'. " Seats: $ticketBooking->seat_number" ,
                'trx' => 'TRX-'.rand(000000,999999).rand(000000,999999)
            ]);


            if($ticketBooking){

                            // $msg =  "Your Ticket ".$ticketBooking->tripRoute->name." ".date('D d M Y h:i A', strtotime($ticketBooking->booking_date))." (".$ticketBooking->tripRoute->start_point_name."-".$ticketBooking->tripRoute->end_point_name.") PNR: ".$pnr." Seats:".$request->seat_number." Confirm request successful. By Shadin Travels";

            // $msg =  "Your Ticket ".$ticketBooking->tripRoute->name." PNR: ".$pnr." Seats:".$request->seat_number." Confirmed. By Shadin Travels";
            // send_sms($phone, $msg);

                $pdfname = $ticketBooking->pnr.'_'.time().'.pdf';
                $logPdf['ticket_id'] = $ticketBooking->id;
                $logPdf['pdf_name'] = $pdfname;
                $logPdf['token'] = strtolower(str_random(60));
                $pdfId = LogPdf::create($logPdf)->id;
                $pdfToken = LogPdf::where('id',$pdfId)->first();
                return response()->json(['pnr'=> route('ticket.print',[$pdfToken->token])]);
            }

//        }

    }

    public function checkedSeatPending(Request $request)
    {
      $trip_id = DB::table('pendting_table')->where('trip_id', $request->id_no)->value('trip_id');
      if($trip_id)
      {
        DB::table('pendting_table')
            ->where('trip_id', $trip_id)
            ->update(['seat_number' => $request->seat_number]);
      }
      else {
        DB::table('pendting_table')->insert(
          ['trip_id' => $request->id_no, 'seat_number' => $request->seat_number]
        );
      }


    }


    /*
    *
    */
    public function deposit()
    {
            $data['page_title'] = " Payment Methods";
            $data['gates'] = Gateway::whereStatus(1)->get();
            return view('agent.deposit', $data);
    }

    public function depositDataInsert(Request $request)
    {
        $this->validate($request, [
            'amount' => 'required|numeric|min:1',
            'gateway' => 'required',
        ]);
       $user =  Auth::guard('agent')->user();

        if ($request->amount <= 0) {
            return back()->with('danger', 'Invalid Amount');
        } else {
            $gate = Gateway::findOrFail($request->gateway);

            if (isset($gate)) {
                if ($gate->minamo <= $request->amount && $gate->maxamo >= $request->amount) {
                    $charge = $gate->fixed_charge + ($request->amount * $gate->percent_charge / 100);
                    $usdamo = ($request->amount + $charge) / $gate->rate;

                    $depo['user_id'] = $user->id;
                    $depo['gateway_id'] = $gate->id;
                    $depo['amount'] = $request->amount;
                    $depo['charge'] = $charge;
                    $depo['usd'] = round($usdamo, 2);
                    $depo['btc_amo'] = 0;
                    $depo['btc_wallet'] = "";
                    $depo['trx'] = 'TRX-'.rand(000000,999999).rand(000000,999999);
                    $depo['try'] = 0;
                    $depo['status'] = 0;
                    $depo['role'] = 2;
                    Deposit::create($depo);

                    Session::put('Track', $depo['trx']);
                    return redirect()->route('user.deposit.preview');

                } else {
                    return back()->with('danger', 'Please Follow Deposit Limit');
                }
            } else {
                return back()->with('danger', 'Please Select Deposit gateway');
            }
        }
    }

    public function depositPreview()
    {

        $track = Session::get('Track');
        $data = Deposit::where('status', 0)->where('trx', $track)->first();
        $page_title = "Deposit Preview";
        $auth = Auth::guard('agent')->user();
        return view('agent.payment.preview', compact('data', 'page_title','role'));
    }


    public function activity()
    {
        $user = Auth::guard('agent')->user();
        $data['invests'] = Trx::where('user_id',$user->id)->where('role',2)->latest()->paginate(15);
        $data['page_title'] = "Transaction Log";
        return view('agent.trx', $data);
    }

    public function depositLog()
    {
        $user = Auth::guard('agent')->user();
        $data['invests'] = Deposit::where('user_id',$user->id)->whereStatus(1)->where('role',2)->latest()->paginate(20);
        $data['page_title'] = "Deposit Log";
        return view('agent.deposit-log', $data);
    }

    public function ticketLog()
    {
        $user = Auth::guard('agent')->user();
        $data['page_title'] = "My Ticket Purchase Log";
        $data['invests'] = TicketBooking::where('agent_id',$user->id)->where('payment_status',1)->latest()->paginate(15);
        return view('agent.ticket-log', $data);
    }

    public function changePassword()
    {
        $data['page_title'] = "Change Password";
        return view('agent.change_password',$data);
    }

    public function updatePassword(Request $request)
    {
        $request->validate([
            'old_password' => 'required',
            'new_password' => 'required|min:5',
            'password_confirmation' => 'required|same:new_password',
        ]);

        $user = Auth::guard('agent')->user();

        $oldPassword = $request->old_password;
        $password = $request->new_password;
        $passwordConf = $request->password_confirmation;

        if (!Hash::check($oldPassword, $user->password) || $password != $passwordConf) {
            $notification =  array('message' => 'Password Do not match !!', 'alert-type' => 'error');
            return back()->with($notification);
        }elseif (Hash::check($oldPassword, $user->password) && $password == $passwordConf)
        {
            $user->password = bcrypt($password);
            $user->save();
            $notification =  array('message' => 'Password Changed Successfully !!', 'alert-type' => 'success');
            return back()->with($notification);
        }
    }

    public function profile()
    {
        $data['admin'] = Auth::guard('agent')->user();
        $data['page_title'] = "Profile Settings";
        return view('agent.profile',$data);
    }

    public function updateProfile(Request $request)
    {
        $data = Agent::findOrFail($request->id);
        $request->validate([
            'first_name' => 'required|max:20',
            'last_name' => 'required|max:20',
            'email' => 'required|max:50|unique:agents,email,'.$data->id,
            'phone' => 'required',
        ]);

        $in = Input::except('_method','_token','balance','username','image');
        if($request->hasFile('image')){
            $image = $request->file('image');
            $filename = 'agent_'.time().'.jpg';
            $location = 'assets/images/agent/' . $filename;

            Image::make($image)->resize(300,300)->save($location);
            $path = '.assets/images/agent/';
            File::delete($path.$data->picture);
            $in['picture'] = $filename;
        }
        $data->fill($in)->save();

        $notification =  array('message' => 'Profile Update Successfully', 'alert-type' => 'success');
        return back()->with($notification);
    }

    public function logout()    {
        Auth::guard('agent')->logout();
        session()->flash('success', 'You have been logged out!!');
        return redirect('/agent');
    }
}
