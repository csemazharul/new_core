<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RideInfo extends Model
{
  protected $guarded = [];

  protected $table ="ride_infos";

}
